﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class KLidarBoundSim : MonoBehaviour
{
    public struct KRayInfo
    {
        public uint rayId;
        public uint rayLoop;
        public Vector3 intersection;

        public void Init(uint id, uint loop)
        {
            rayId = id;
            rayLoop = loop;
            intersection = new Vector3(-1, -1, -1);
        }
    }


    /*
     * constance
     */

    const float rayRadius = 100.0f;


    /*
     * public members
     */

    public int rayRoundCount = 1024;
    public int rayBeamCount = 32;
    public float rayBeamAngle = 90.0f;
    public int scopeDistance = 30;

    public bool drawPointCloud = true;

    int coreAlgorithmLoop = 0;
    bool drawRay = false;


    /*
	 * core members
	 */

    List<Ray> rayList;
    List<KRayInfo> rayResult;

    // aids
    List<GameObject> envObjects;


    /*
     * constructor
     */

    void Start()
    {
        rayList = new List<Ray>();
        rayResult = new List<KRayInfo>();
        envObjects = new List<GameObject>();
    }

    void OnDisable()
    {
        Reset();
    }

    void Reset()
    {
        if (rayList != null)
            rayList.Clear();

        if (rayResult != null)
            rayResult.Clear();

        if (envObjects != null)
            envObjects.Clear();
        
        coreAlgorithmLoop = 0;
    }

    void Update()
    {
        Reset();

        GenerateRaysList();

        CalculateIntersectionWithBounds();

        PrintRayResults();
    }


    /*
     * operations
     */

    void GenerateRaysList()
    { 
        Vector3 origin = this.transform.position; 
        Vector3 direction = new Vector3(0, 0, 1); 

        for (int i = 0; i < rayRoundCount; i++)
        { 
            // rotate the direction angleHorizonto degree along Y axis 
            float angleHorizonto = 360.0f * i / rayRoundCount; 
            direction = Quaternion.AngleAxis(angleHorizonto, Vector3.up) * direction; 

            // get normal of direction and up 
            Vector3 normal1 = Vector3.Cross(direction, Vector3.up); 

            for (int j = 0; j < rayBeamCount; j++)
            { 
                // rotate the direction angleVerticle degree along normal1 axis 
                float angleVerticle = rayBeamAngle * ((float)j / (float)rayBeamCount - 0.5f); 
                Vector3 newDirection = Quaternion.AngleAxis(angleVerticle, normal1) * direction; 

                // get ray 
                Ray ray = new Ray(origin, newDirection); 
                rayList.Add(ray); 
            } 
        } 
    }

    IList<GameObject> GenerateObjectsInScope()
    {
        List<GameObject> objectList = new List<GameObject>();
        {
            GameObject[] objectArray = GameObject.FindGameObjectsWithTag("LidarVisible");

            foreach (GameObject obj in objectArray)
            {
                if (obj.name != "aCube")
                    continue;                

                Collider collider = obj.GetComponent<BoxCollider>();

                if (collider == null)
                {
                    collider = obj.GetComponent<CapsuleCollider>();
                }

                if (collider == null)
                {
                    Debug.LogWarning("target doesn't have a collider component: " + obj.name);
                    continue;
                }

                Vector3 center = collider.bounds.center;

                if (Vector3.Distance(center, this.transform.position) < scopeDistance)
                {
                    objectList.Add(obj);
                }
            }
        }
        return objectList;
    }

    void CalculateIntersectionWithBounds()
    {
        RaycastHit hit;

        for (int i = 0; i < rayList.Count; i++)
        {
            Ray ray = rayList[i];

            if (Physics.Raycast(ray, out hit, 30)) //  && hit.transform.gameObject == target)
            {
                KRayInfo info = new KRayInfo();
                info.rayId = (uint)i;
                info.intersection = hit.point;
                rayResult.Add(info);

                //Debug.Log ("hit " + target.name + " at " + hit.point);
            }
        }
    }

    float lastTime = -1;

    void PrintRayResults()
    {
        if (Time.time - lastTime > 1)
        {
            Debug.Log("Core Algorithm Loop Num = " + coreAlgorithmLoop);
            Debug.Log("Total points = " + rayResult.Count);
            lastTime = Time.time;
        }
    }

    void OnDrawGizmosSelected()
    {
        if (this != null && rayResult != null)
        {
            foreach (KRayInfo info in rayResult)
            {
                if (drawPointCloud)
                {
                    Vector3 point = info.intersection;
                    Gizmos.DrawCube(point, new Vector3(0.1f, 0.1f, 0.1f));
                }

                if (drawRay)
                {
                    Ray ray = rayList[(int)info.rayId];
                    Debug.DrawRay(ray.origin, ray.direction * 30, Color.green);
                }
            }
        }
    }
}
