﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct KTriangle
{
    public Vector3 vertA;
    public Vector3 vertB;
    public Vector3 vertC;

    public void Init(Vector3 a, Vector3 b, Vector3 c)
    {
        vertA = a;
        vertB = b;
        vertC = c;
    }
}


public struct KIndexInfo
{
    public Matrix4x4 localToWorldMatrix;
    public int v_start;
    public int v_end;
    public int t_start;
    public int t_end;
    public int r_start;
    public int r_end;

    public static int Size
    {
        get
        {
            return sizeof(int) * 6 + sizeof(float) * 16;
        }
    }

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(" (polygon=" + (t_end - t_start + 1) / 3 + ") \nv=[" + v_start + " " + v_end + "] t=[" + t_start + " " + t_end + "] r=[" + r_start + " " + r_end + "]");
        return sb.ToString();
    }
}


public struct KRayResult
{
    /* 
     * main output
     */

    // intersection between ray and triangle
    public Vector3 intersection;

    /*
     * debugging output
     */

    // how many times to compute distance
    public uint computeDistanceTimes;

    // how many times to compute intersection
    public uint computeIntersectionTimes;

    // error code : [0 = no error; 1 = id error; ]
    public uint errorCode;

    // distance between ray and triangle
    public float distance;

    public static int Size
    {
        get
        {
            return sizeof(float) * 3 + sizeof(uint) * 3 + sizeof(float) * 1;
        }
    }

    public void Reset()
    {
        intersection = Vector3.zero;

        computeDistanceTimes = 0;
        computeIntersectionTimes = 0;
        errorCode = 0;
        distance = int.MaxValue;
    }
};




