﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class KObjectChecker
{
    public static bool CheckValidation(GameObject obj, Vector3 rayPos, float rayRadius)
    {
        /*
         * return:  true means this obj is valid, and has been added to WVMap, no matter added at this time or before
         */

        // 1. check obj
        if (obj == null)
        {
            Debug.LogError("encounter null game object");
            return false;
        }

        // 3. check obj collider component
        Collider collider = obj.GetComponent<BoxCollider>();
        if (collider == null)
        {
            collider = obj.GetComponent<CapsuleCollider>();
        }
        if (collider == null)
        {
            collider = obj.GetComponent<SphereCollider>();
        }
        if (collider == null)
        {
            //Debug.LogWarning("cannot be added to WVmap, since no collider component: " + obj.name);
            return false;
        }

        // 4. check distance
        {
            /*
             * In what condition, object A is considered invisiable for ray origin point R?
             * 1) distance between A and R (|AR|) is longer than A's radius (aRadius) + R's radius (rayRadius)
             * 
             * which means:
             * |AR| > aRadius + rayRadius
             * 
             * for simplifying the calculation:
             * 1) aRadius = Max (bound.x, bound.y, bound.z)
             * 2) |AR| > aRadius + rayRadius will be transformed to sqr(OA) > sqr(aRadius + rayRadius), or OA.sqrMagnitude > sqr(aRadius + rayRadius)
             * 
             */

            Vector3 extents = collider.bounds.extents;
            float aRadius = Mathf.Max(Mathf.Max(extents.x, extents.y), extents.z);

            Vector3 AR = collider.bounds.center - rayPos;
            if (AR.sqrMagnitude > (aRadius + rayRadius) * (aRadius + rayRadius))
            {
                //Debug.LogWarning("cannot be added to WVmap, since too far away from : " + obj.name);
                return false;
            }
        }

        // 5. check obj transform component
        Transform transform = obj.transform;
        if (transform == null)
        {
            //Debug.LogWarning("cannot be added to WVmap, since no transform component : " + obj.name);
            return false;
        }

        // 6. check meshFilter component 
        // yikang p3: check whether can find other mesh component like meshFilter
        MeshFilter meshFilter = obj.GetComponent<MeshFilter>();
        if (meshFilter == null)
        {
            //Debug.LogWarning("cannot be added to WVmap, since no meshFilter filter: " + obj.name);
            return false;
        }

        // 7. check mesh component
        Mesh mesh = meshFilter.sharedMesh;
        if (mesh == null)
        {
            //Debug.LogWarning("cannot be added to WVmap, since no mesh filter: " + obj.name);
            return false;
        }
        else if (!mesh.isReadable)
        {
            //Debug.LogWarning("cannot be added to WVmap, since mesh is NOT readable : " + obj.name);
            return false;
        }

        return true;
    }

    public static Mesh GetMesh(GameObject obj)
    {
        MeshFilter meshFilter = obj.GetComponent<MeshFilter>();
        if (meshFilter == null)
        {
            Debug.LogError("meshFilter is null");
            return null;
        }

        return meshFilter.sharedMesh;
    }

    public static Matrix4x4 GetRendererTransformMatrix(GameObject obj)
    {
        /*
        Transform transform = obj.GetComponent<Transform>();
        if (transform == null)
        {
            Debug.LogError("transform is null");
            return Matrix4x4.zero;
        }

        return transform.localToWorldMatrix;
*/

        Renderer renderer = obj.GetComponent<Renderer>();
        if (renderer == null)
        {
            Debug.LogError("renderer is null");
            return Matrix4x4.zero;
        }

        return renderer.localToWorldMatrix;
    }
}
