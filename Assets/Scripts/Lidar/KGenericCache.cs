﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class KGenericCache<T>
{
    int validCount = 0;

    public int ValidCount
    {
        get
        {
            return validCount;
        }
        set
        {
            validCount = value;
        }
    }

    string name = "";

    public string Name
    {
        get
        {
            return name;
        }
    }

    T[] cache;

    public T[] Cache
    {
        get
        {
            return cache;
        }
    }

    int allocateTime;

    public int AllocateTime
    {
        get
        {
            return allocateTime;
        }
    }

    // how many more objects each time to allocate more memory
    int objectsNumPerAllocation = 0;


    /*
     * constructor and public
     */

    public KGenericCache(int objectsNumPerAllocation, string name = "")
    {
        this.name = name;
        this.objectsNumPerAllocation = objectsNumPerAllocation;
        this.cache = Allocate(this.objectsNumPerAllocation);
    }

    public void Copy(T[] comingArray)
    {
        int leftCapacity = this.cache.Length - this.validCount;

        if (leftCapacity >= comingArray.Length)
        {
            // do nothing
        }
        else
        {
            // create new cache
            int newCapacity = (comingArray.Length < leftCapacity + this.objectsNumPerAllocation) ? 
                (this.cache.Length + this.objectsNumPerAllocation) : (this.cache.Length + comingArray.Length);
            T[] newCache = Allocate(newCapacity);

            // copy cache to new cache
            BlockCopy(this.cache, newCache, 0);

            // assign cache
            this.cache = newCache;
        }

        // do block copy
        BlockCopy(comingArray, this.cache, this.validCount);

        // update count
        this.validCount += comingArray.Length;
    }

    public void ClearCount()
    {
        validCount = 0;
    }

    public void ReleaseMemory()
    {
        cache = null;
    }

    public void Add(T item)
    {
        // check space
        if (this.validCount > this.cache.Length)
        {
            Debug.LogError("In " + name + " something wrong for count: validCount = " + validCount + ", cache.Length = " + this.cache.Length);
            return;
        }
        else if (this.validCount == this.cache.Length)
        {
            AllocateMoreCapacity();
        }

        // do real add
        this.cache[this.validCount] = item;
        this.validCount++;
    }

    public void AllocateMoreCapacity()
    {
        // create new cache
        T[] newCache = Allocate(this.cache.Length + this.objectsNumPerAllocation);    

        // copy cache to new cache
        BlockCopy(this.cache, newCache, 0);

        // assign cache
        this.cache = newCache;
    }

    /*
     * private
     */

    T[] Allocate(int capacity)
    {
        allocateTime++;
        Debug.LogWarning(name + " has been allocated with " + capacity + " unit size; \nand its allocation time is : " + allocateTime);
        return new T[capacity];
    }

    void BlockCopy(T[] src, T[] dst, int dstOffset)
    {
        if (src.Length + dstOffset < dst.Length)
        {
            Array.Copy(src, 0, dst, dstOffset, src.Length);

            // yikang p3: check whether Buffer.BlockCopy is better
            //Buffer.BlockCopy(src, 0, dst, dstOffset * memorySizePerObject, src.Length * memorySizePerObject);
        }
        else
        {
            Debug.LogError(name + "'s block copy failed, since dst space is not enough for src length: " + src.Length);
        }
    }
}
