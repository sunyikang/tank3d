﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class KLidarMeshSim
{
    /*
     * constances
     */

    // gpu entrance
    const string gpuP1Name_meshRaycastMain = "MeshRaycastMain";

    // gpu vertices
    const string gpuP2Name_vertices = "vertices";

    // gpu triangles
    const string gpuP3Name_triangles = "triangles";

    // gpu rays
    const string gpuP4Name_rayRadius = "rayRadius";
    const string gpuP5Name_rayDirs = "rayDirs";
    const string gpuP6Name_rayPos = "rayPos";
    const string gpuP7Name_raysCount = "raysCount";

    // gpu threads
    const string gpuP8Name_gGroudID_XCount = "gGroudID_XCount";
    const string gpuP9Name_gGroudID_YCount = "gGroudID_YCount";
    const int gpuThreadCountPerGroup = 16;

    // index infos
    const string gpuP10Name_indexInfos = "indexInfos";

    // rayResults
    const string gpuP11Name_rayResults = "rayResults";

    const int predictedObjectsNum = 50;

    const int predictedVerticesNum = 50000;

    const int predictedTriangleIndexNum = 100000;

    const int predictedRaysNum = 30000;

    // other
    const string gLidarVisibleTag = "LidarVisible";
    const string gLidarVisibleFloorTag = "LidarVisibleFloor";


    /*
     * runtime parameters
     */

    int rayRoundCount = 1024;
    int rayBeamCount = 32;
    float rayBeamAngle = 90.0f;
    int rayRadius = 30;
    int secondsToPrintInfo = 4;
    bool ignoreDuplicateRay = true;

    /*
     * members
     */

    // input
    Vector3 rayPos;
    KGenericCache<KIndexInfo> indexInfos;
    KGenericCache<Vector3> rayDirs;
    KGenericCache<Vector3> vertices;
    KGenericCache<int> triangles;

    // output
    KGenericCache<KRayResult> rayResults;
    Dictionary<Vector3, Vector3> pointResults;

    // aids
    ComputeShader shader;
    List<string> detectedObjectNameList;


    /*
     * constructor and public operations
     */

    public KLidarMeshSim(ComputeShader shader)
    {
        // input
        indexInfos = new KGenericCache<KIndexInfo>(predictedObjectsNum, "indexInfos");
        rayDirs = new KGenericCache<Vector3>(predictedRaysNum, "rayDirs");
        vertices = new KGenericCache<Vector3>(predictedVerticesNum, "vertices");
        triangles = new KGenericCache<int>(predictedTriangleIndexNum, "triangles");

        // output
        rayResults = new KGenericCache<KRayResult>(predictedRaysNum, "rayResults");
        pointResults = new Dictionary<Vector3, Vector3>(predictedRaysNum);

        // aid
        this.shader = shader;
        detectedObjectNameList = new List<string>();

        SetGpuStaticMember();
    }

    public void Update(Vector3 rayPos, int rayRoundCount, int rayBeamCount, float rayBeamAngle, int rayRadius, bool computeByGPU, bool ignoreDuplicateRay)
    {
        SetParameters(rayPos, rayRoundCount, rayBeamCount, rayBeamAngle, rayRadius, ignoreDuplicateRay);
        
        Reset();

        if (computeByGPU)
        {
            PrepareDataForGpu();
            ComputeMeshTrianglesInGpu();
        }
        else
        {
            PrepareDataForCpu();
            ComputeMeshTrianglesInCpu();
        }

        if (ignoreDuplicateRay)
        {
            ConvertRayResultsToPointResults();
        }
    }

    public void Reset()
    {
        // input
        indexInfos.ClearCount();
        rayDirs.ClearCount();
        vertices.ClearCount();
        triangles.ClearCount();

        // output
        rayResults.ClearCount();
        pointResults.Clear();

        // aids
        detectedObjectNameList.Clear();
    }

    public void Destroy()
    {
        // input
        indexInfos.ReleaseMemory();
        rayDirs.ReleaseMemory();
        vertices.ReleaseMemory();
        triangles.ReleaseMemory();

        // output
        rayResults.ReleaseMemory();

        // aids
        detectedObjectNameList.Clear();
    }

    public void DrawPointCloud(float pointCubeSize)
    {
        if (ignoreDuplicateRay)
        {
            foreach (KeyValuePair<Vector3, Vector3> item in pointResults)
            {
                Vector3 point = item.Value;
                Gizmos.DrawCube(point, new Vector3(pointCubeSize, pointCubeSize, pointCubeSize));
            }
        }
        else
        {
            if (rayResults.ValidCount == 0)
                return;

            if (rayResults.ValidCount > rayResults.Cache.Length)
            {
                Debug.LogError("rayResults valid count=" + rayResults.ValidCount + ", which is bigger than real capacity=" + rayResults.Cache.Length);
                return;
            }

            for (int i = 0; i < rayResults.ValidCount; i++)
            {
                Vector3 point = rayResults.Cache[i].intersection;
                Gizmos.DrawCube(point, new Vector3(pointCubeSize, pointCubeSize, pointCubeSize));
            }
        }
    }

    public void DrawValidRays()
    {
        for (int i = 0; i < rayDirs.ValidCount; i++)
        {
            Vector3 dir = rayDirs.Cache[i];
            Debug.DrawRay(rayPos, dir * rayRadius, Color.green);
        }
    }

    float tempTimeAtPrintInfo = int.MinValue / 2;

    public void PrintRayDebugInfo()
    {
        if (Time.time - tempTimeAtPrintInfo > secondsToPrintInfo)
        {
            tempTimeAtPrintInfo = Time.time;

            // 0) print index info
            PrintIndexInfos();

            // 1) print rays: check performance:   rayCount, loop --> bottleneck suspecious
            PrintRayResults();
        }
    }


    /*
     * private functions
     */

    void SetParameters(Vector3 rayPos, int rayRoundCount, int rayBeamCount, float rayBeamAngle, int rayRadius, bool ignoreDuplicateRay)
    {
        this.rayPos = rayPos;
        this.rayRoundCount = rayRoundCount;
        this.rayBeamCount = rayBeamCount;
        this.rayBeamAngle = rayBeamAngle;
        this.rayRadius = rayRadius;
        this.ignoreDuplicateRay = ignoreDuplicateRay;
    }

    void SetGpuStaticMember()
    {
        shader.SetVector("gInvalidPoint", Vector3.zero);
    }

    void PrepareDataForGpu()
    {
        PrepareDataForGpuWithObjTag(gLidarVisibleTag, false);
        PrepareDataForGpuWithObjTag(gLidarVisibleFloorTag, true);

        // for testing
        //PrepareDataForGpuWithObjName("aCube", false);
        //PrepareDataForGpuWithObjName("Building01 (1)", false);
    }

    void PrepareDataForGpuWithObjTag(string objTag, bool towardFloor)
    {
        GameObject[] allVisibleObjects = GameObject.FindGameObjectsWithTag(objTag);

        foreach (GameObject obj in allVisibleObjects)
        {
            if (KObjectChecker.CheckValidation(obj, this.rayPos, this.rayRadius))
            {
                FillIn(obj, towardFloor);
            }
        }
    }

    void PrepareDataForGpuWithObjName(string objName, bool towardFloor)
    {
        GameObject obj = GameObject.Find(objName);
        if (obj != null)
        {
            if (KObjectChecker.CheckValidation(obj, this.rayPos, this.rayRadius))
            {
                FillIn(obj, towardFloor);
            }
        }
    }

    void PrepareDataForCpu()
    {
        //PrepareDataForCpuWithObjName("GroundPlane", true);
        PrepareDataForCpuWithObjName("aCube", false);
        PrepareDataForCpuWithObjName("aCube1", false);
        PrepareDataForCpuWithObjName("aCube2", false);
        PrepareDataForCpuWithObjName("aCube3", false);
        PrepareDataForCpuWithObjName("aCube4", false);
    }

    void PrepareDataForCpuWithObjName(string objName, bool towardFloor)
    {
        GameObject obj = GameObject.Find(objName);
        if (obj != null)
        {
            FillIn(obj, towardFloor);
        }
    }

    void FillIn(GameObject obj, bool forRayTowardFloor = false)
    {
        KIndexInfo info = new KIndexInfo();
        {
            FillInObjectName(obj);
            FillInTransforms(obj, ref info);
            FillInRays(obj, forRayTowardFloor, ref info);
            FillInVerticesAndTriangles(obj, ref info);
        }
        indexInfos.Add(info);
    }

    void FillInObjectName(GameObject obj)
    {
        detectedObjectNameList.Add(obj.name);
    }

    void FillInTransforms(GameObject obj, ref KIndexInfo info)
    {
        info.localToWorldMatrix = KObjectChecker.GetRendererTransformMatrix(obj);
    }

    void FillInRays(GameObject obj, bool forRayTowardFloor, ref KIndexInfo info)
    {
        // fill in rays, and update indexInfoList
        info.r_start = rayDirs.ValidCount;
        {
            if (forRayTowardFloor)
                FillInRaysIntersectWithFloor(obj);
            else
                FillInRaysIntersectWithObjectBound(obj);
        }
        info.r_end = rayDirs.ValidCount - 1;
    }

    void FillInRaysIntersectWithObjectBound(GameObject target)
    {
        if (target == null)
        {
            Debug.LogError("encounter null game object");
            return;
        }

        Transform transform = target.transform;
        if (transform == null)
        {
            Debug.LogError(target.name + " doesn't have mesh filter or transform");
            return;
        }

        Collider collider = target.GetComponent<BoxCollider>();
        if (collider == null)
        {
            collider = target.GetComponent<CapsuleCollider>();
        }

        if (collider == null)
        {
            Debug.LogError("target doesn't have a collider component");
            return;
        }

        Vector3 center = collider.bounds.center;
        Vector3 extents = collider.bounds.extents;

        // angles on XZ panel's projection
        int roundMinId = 0;
        int roundMaxId = 0;
        CalculateRoundIdScope(center, extents, out roundMinId, out roundMaxId);

        // angles on Y+Ray panel's projection
        int beamMinId = 0;
        int beamMaxId = 0;
        CalculateBeamIdScope(center, extents, out beamMinId, out beamMaxId);
        //Debug.Log("round min and max id: [" + roundMinId + ", " + roundMaxId + "] beam min and max id: [" + beamMinId + ", " + beamMaxId + "]");

        FillInRaysInFrustum(roundMinId, roundMaxId, beamMinId, beamMaxId);
    }

    void CalculateRoundIdScope(Vector3 center, Vector3 extents, out int roundMinId, out int roundMaxId)
    {
        /*
         * the idea is to project bounding box into XZ panel; and
         * calculate in XZ panel what the round max and min angle are;
         * then calculate the round max and min ID are.
         */
      
        float radiusXZ = Mathf.Sqrt(extents.x * extents.x + extents.z * extents.z);
        float distance = Vector2.Distance(new Vector2(center.x, center.z), new Vector2(rayPos.x, rayPos.z));

        if (radiusXZ >= distance)
        {
            // yikang p3: Ego car is not supposed to be inside another object now! Let's consider this situation in future...
            roundMinId = 0;
            roundMaxId = -1;
        }
        else
        {
            float offsetAngle = 0.0f;
            {
                offsetAngle = Mathf.Asin(radiusXZ / distance) * 180.0f / Mathf.PI;
                if (offsetAngle < 0 || offsetAngle > 90)
                {
                    Debug.LogError("offsetAngle is wrong: " + offsetAngle);
                }
            }

            float centerLineAngle = 0.0f; 
            { 
                Vector2 boxPosXZ = new Vector2(center.x, center.z); 
                Vector2 rayPosXZ = new Vector2(rayPos.x, rayPos.z); 
                Vector2 direction = boxPosXZ - rayPosXZ; 
                centerLineAngle = Vector2.Angle(Vector2.right, direction); 

                if (direction.y < 0)
                { 
                    centerLineAngle = 360 - centerLineAngle; 
                }
            } 

            float minDegreeInXZPlane = centerLineAngle - offsetAngle;
            {
                minDegreeInXZPlane = minDegreeInXZPlane % 360;
                if (minDegreeInXZPlane < 0)
                    minDegreeInXZPlane += 360;
                if (minDegreeInXZPlane < 0 && minDegreeInXZPlane > 360)
                {
                    Debug.LogError("minAngle = " + minDegreeInXZPlane);
                }
            }

            float maxDegreeInXZPlane = centerLineAngle + offsetAngle;
            {
                maxDegreeInXZPlane = maxDegreeInXZPlane % 360;
                if (maxDegreeInXZPlane < 0)
                    maxDegreeInXZPlane += 360;
                if (maxDegreeInXZPlane < 0 && maxDegreeInXZPlane > 360)
                {
                    Debug.LogError("maxAngle = " + maxDegreeInXZPlane);
                }
            }

            roundMinId = CalculateRoundId(minDegreeInXZPlane);
            roundMaxId = CalculateRoundId(maxDegreeInXZPlane);
        }
    }

    int CalculateRoundId(float degreeInXZPlane)
    {
        int roundId = (int)(degreeInXZPlane * rayRoundCount / 360.0f);
        if (roundId < 0)
        {
            Debug.LogWarning("roundId is smaller than 0: " + roundId + " degree = " + degreeInXZPlane);
            roundId = 0;
        }
        else if (roundId >= rayRoundCount)
        {
            Debug.LogWarning("roundId is bigger than " + rayRoundCount + " : " + roundId + " degree = " + degreeInXZPlane);
            roundId = rayRoundCount - 1;
        }
        return roundId;
    }

    void CalculateBeamIdScope(Vector3 center, Vector3 extents, out int beamMinId, out int beamMaxId)
    {
        float radiusXY = Mathf.Sqrt(extents.x * extents.x + extents.y * extents.y);
        float distance = Vector3.Distance(center, rayPos);

        float offsetAngle = Mathf.Atan(radiusXY / distance) * 180.0f / Mathf.PI;
        if (offsetAngle < 0)
        {
            Debug.LogWarning("round angle1 is negative: " + offsetAngle);
        }

        float centerLineAngle = Mathf.Asin((center.y - rayPos.y) / distance) * 180.0f / Mathf.PI;
        if (centerLineAngle < -90.0f && centerLineAngle > 90.0f)
        {
            Debug.LogWarning("round angle2 is out of scope: " + centerLineAngle);
        }

        beamMinId = CalculateBeamId(centerLineAngle - offsetAngle, rayBeamAngle, rayBeamCount);
        beamMaxId = CalculateBeamId(centerLineAngle + offsetAngle, rayBeamAngle, rayBeamCount);
    }

    int CalculateBeamId(float degreeInXYPlane, float rayBeamAngle, int rayBeamCount)
    {
        int beamId = (int)((degreeInXYPlane / rayBeamAngle + 0.5f) * rayBeamCount);
        if (beamId < 0)
        {
            //Debug.LogWarning("beamId is smaller than 0: " + beamId);
            beamId = 0;
        }
        else if (beamId >= rayBeamCount)
        {
            //Debug.LogWarning("beamId is bigger than " + rayBeamCount + " : " + beamId);
            beamId = rayBeamCount - 1;
        }
        return beamId;
    }

    void FillInRaysInFrustum(int roundMin, int roundMax, int beamMin, int beamMax)
    {
        if (roundMin <= roundMax)
        {
            for (int roundId = roundMin; roundId <= roundMax; roundId++)
            {
                FillInRaysInFrustrum(roundId, beamMin, beamMax);
            }
        }
        else
        {
            /*
             * note: to deal with the situation that roundMin = 1022, roundMax = 2
             * by given rayRoundCount = 1024.
             */
            for (int roundId = 0; roundId <= roundMax; roundId++)
            {
                FillInRaysInFrustrum(roundId, beamMin, beamMax);
            }
            for (int roundId = roundMin; roundId <= rayRoundCount - 1; roundId++)
            {
                FillInRaysInFrustrum(roundId, beamMin, beamMax);
            }
        }
    }

    void FillInRaysInFrustrum(int roundId, int beamMin, int beamMax)
    {
        // initial direction
        Vector3 direction = Vector3.right;  // better than using new Vector3(1, 0, 0);

        // rotate the direction angleHorizonto degree along Y axis
        float angleHorizonto = 360.0f * roundId / rayRoundCount;
        direction = Quaternion.AngleAxis(angleHorizonto, Vector3.down) * direction;

        // get normal of direction and up
        Vector3 normal = Vector3.Cross(direction, Vector3.up);

        for (int j = beamMin; j <= beamMax; j++)
        {
            // rotate the direction angleVerticle degree along normal1 axis
            float angleVerticle = rayBeamAngle * ((float)j / (float)rayBeamCount - 0.5f);
            Vector3 rayDir = Quaternion.AngleAxis(angleVerticle, normal) * direction;

            // fill in
            rayDirs.Add(rayDir);
        }
    }

    void FillInRaysIntersectWithFloor(GameObject target)
    {
        /*
         * fill in rays that is inside the rays' range (radius)
         * 
         *      theta = arccos(rayHeight / rayRadius)
         * 
         *      maxBeamId = beamCount * (theta / PI)
         * 
         */

        if (target == null)
        {
            Debug.LogError("encounter null game object");
            return;
        }

        Transform transform = target.transform;
        if (transform == null)
        {
            Debug.LogError(target.name + " doesn't have mesh filter or transform");
            return;
        }

        Collider collider = target.GetComponent<BoxCollider>();
        if (collider == null)
        {
            collider = target.GetComponent<CapsuleCollider>();
        }

        if (collider == null)
        {
            Debug.LogError("target doesn't have a collider component");
            return;
        }

        if (rayPos.y <= 0)
        {
            // don't deal with ray on floor when ray pos is below the floor
            return;
        }

        float theta = Mathf.Acos(rayPos.y / this.rayRadius);

        int maxBeamId = (int)(this.rayBeamCount * (theta / Mathf.PI));

        FillInRaysInCenterLineBetaAngleToFloor(maxBeamId);
    }

    void FillInRaysInCenterLineBetaAngleToFloor(int maxBeamId)
    {
        for (int i = 0; i < this.rayRoundCount; i++)
        {
            // initial direction
            Vector3 direction = Vector3.right;  // better than using new Vector3(1, 0, 0);

            // rotate the direction angleHorizonto degree along Y axis
            float angleHorizonto = 360.0f * i / rayRoundCount;
            direction = Quaternion.AngleAxis(angleHorizonto, Vector3.down) * direction;

            // get normal of direction and up
            Vector3 normal = Vector3.Cross(direction, Vector3.up);

            for (int j = 0; j < maxBeamId; j++)
            {
                // rotate the direction angleVerticle degree along normal1 axis
                float angleVerticle = rayBeamAngle * ((float)j / (float)rayBeamCount - 0.5f);
                Vector3 rayDir = Quaternion.AngleAxis(angleVerticle, normal) * direction;

                rayDirs.Add(rayDir);
            }
        }
    }

    void FillInVerticesAndTriangles(GameObject obj, ref KIndexInfo info)
    {
        // fill in vertices and triangles, and update indexInfoList

        Mesh mesh = KObjectChecker.GetMesh(obj);
        if (mesh == null)
        {
            Debug.LogError("something wrong, mesh is null");
            return;
        }

        info.v_start = vertices.ValidCount;
        info.t_start = triangles.ValidCount;
        {
            // copy to existing vertices and triangles 
            vertices.Copy(mesh.vertices);
            triangles.Copy(mesh.triangles);
        }
        info.v_end = vertices.ValidCount - 1;
        info.t_end = triangles.ValidCount - 1;
    }

    void ComputeMeshTrianglesInGpu()
    {
        // transfer 1) vertices, 2) triangles, 3) rays, 4) indexInfoList into GPU, and execute

        if (vertices == null || triangles == null || rayDirs == null || indexInfos == null ||
            vertices.ValidCount == 0 || triangles.ValidCount == 0 || rayDirs.ValidCount == 0 || indexInfos.ValidCount == 0)
        {
            //Debug.Log("there is nothing detected in lidar scope, or something wrong");
            return;
        }

        // 1) set entrance
        int kernel = shader.FindKernel(gpuP1Name_meshRaycastMain);

        // 2) set buffer
        ComputeBuffer inputP2_vertices;
        ComputeBuffer inputP3_triangles;
        ComputeBuffer inputP10_indexInfos;
        ComputeBuffer inputP5_rayDirs;
        ComputeBuffer outputP11_rayResults;
        {
            // yikang p3: check whether the sizeof(float) and sizeof(uint) is equal in all device conditions

            // a) create memory for compute buffer
            inputP2_vertices = new ComputeBuffer(vertices.ValidCount, sizeof(float) * 3);
            inputP3_triangles = new ComputeBuffer(triangles.ValidCount, sizeof(int));
            inputP5_rayDirs = new ComputeBuffer(rayDirs.ValidCount, sizeof(float) * 3);
            inputP10_indexInfos = new ComputeBuffer(indexInfos.ValidCount, KIndexInfo.Size);
            outputP11_rayResults = new ComputeBuffer(rayDirs.ValidCount, KRayResult.Size);

            // b) set data to compute buffer
            inputP2_vertices.SetData(vertices.Cache);
            inputP3_triangles.SetData(triangles.Cache);
            inputP5_rayDirs.SetData(rayDirs.Cache);
            inputP10_indexInfos.SetData(indexInfos.Cache);

            // c) set compute buffer to GPU side (compute shader)
            shader.SetBuffer(kernel, gpuP2Name_vertices, inputP2_vertices);
            shader.SetBuffer(kernel, gpuP3Name_triangles, inputP3_triangles);
            shader.SetBuffer(kernel, gpuP5Name_rayDirs, inputP5_rayDirs);
            shader.SetBuffer(kernel, gpuP10Name_indexInfos, inputP10_indexInfos);
            shader.SetBuffer(kernel, gpuP11Name_rayResults, outputP11_rayResults);
        }

        // 3) set other parameters
        int gpuGroupCount = rayDirs.ValidCount / gpuThreadCountPerGroup + 1;
        {
            shader.SetFloat(gpuP4Name_rayRadius, this.rayRadius);
            shader.SetFloats(gpuP6Name_rayPos, new float[] { rayPos.x, rayPos.y, rayPos.z });
            shader.SetInt(gpuP7Name_raysCount, rayDirs.ValidCount);
            shader.SetInt(gpuP8Name_gGroudID_XCount, gpuGroupCount);
            shader.SetInt(gpuP9Name_gGroudID_YCount, 1);
        }

        // 4) set result capacity
        {
            while (rayResults.Cache.Length < rayDirs.Cache.Length)
            {
                rayResults.AllocateMoreCapacity();
            }
        }

        // verify thread group sizes  1024 / 1 /1 
        {
            //        uint x;
            //        uint y;
            //        uint z;
            //        shader.GetKernelThreadGroupSizes(kernel, out x, out y, out z);
            //        Debug.Log("GetKernelThreadGroupSizes result: " + x + " " + y + " " + z);
        }

        // 5) execute
        shader.Dispatch(kernel, gpuGroupCount, 1, 1);

        // 6) get output
        outputP11_rayResults.GetData(rayResults.Cache);
        rayResults.ValidCount = rayDirs.ValidCount;

        // 7) release buffer
        inputP2_vertices.Release();
        inputP3_triangles.Release();
        inputP5_rayDirs.Release();
        inputP10_indexInfos.Release();
        outputP11_rayResults.Release();
    }

    void ComputeMeshTrianglesInCpu()
    {
        // go through transforms
        for (int objId = 0; objId < indexInfos.ValidCount; objId++)
        {
            KIndexInfo indexInfo = indexInfos.Cache[objId];
            int trianglesLength = indexInfo.t_end - indexInfo.t_start + 1;
            Matrix4x4 localToWorldMatrix = indexInfo.localToWorldMatrix;

            // go through rays
            for (int rayId = indexInfo.r_start; rayId <= indexInfo.r_end; rayId++)
            {
                Vector3 rayDir = rayDirs.Cache[rayId];
                KRayResult result = new KRayResult();
                result.Reset();
                {
                    // temp distance
                    float shortestDistance = this.rayRadius;

                    // go through triangles
                    int trianglesRealCount = trianglesLength / 3;
                    for (int triId = 0; triId < trianglesRealCount; triId++)
                    {
                        // get triangle
                        KTriangle tri;
                        tri.vertA = vertices.Cache[indexInfo.v_start + triangles.Cache[indexInfo.t_start + triId * 3 + 0]];
                        tri.vertB = vertices.Cache[indexInfo.v_start + triangles.Cache[indexInfo.t_start + triId * 3 + 1]];
                        tri.vertC = vertices.Cache[indexInfo.v_start + triangles.Cache[indexInfo.t_start + triId * 3 + 2]];

                        // local to world
                        tri.vertA = LocalToWorld(localToWorldMatrix, tri.vertA);
                        tri.vertB = LocalToWorld(localToWorldMatrix, tri.vertB);
                        tri.vertC = LocalToWorld(localToWorldMatrix, tri.vertC);

                        // compute
                        bool goToCoreLoop = false;
                        Vector3 intersection = Vector3.zero;
                        bool validIntersection = CalculateIntersectionPoint(tri, rayPos, rayDir, ref shortestDistance, out intersection, out goToCoreLoop);

                        // update compute times
                        result.computeDistanceTimes++;
                        result.computeIntersectionTimes += (uint)(goToCoreLoop ? 1 : 0);

                        // update intersection and distance
                        if (validIntersection)
                        {
                            result.intersection = intersection;
                            result.distance = shortestDistance;
                        }
                    }
                }
                rayResults.Add(result);
            }
        }
    }

    Vector3 LocalToWorld(Matrix4x4 localToWorldMatrix, Vector3 localVector)
    {
        return localToWorldMatrix.MultiplyPoint3x4(localVector);
    }

    bool CalculateIntersectionPoint(KTriangle tri, Vector3 rayPos, Vector3 rayDir, ref float shortestDistance, out Vector3 intersection, out bool goToCoreLoop)
    {
        intersection = Vector3.zero;
        goToCoreLoop = false;

        // Triangle
        Vector3 vertA = tri.vertA;
        Vector3 vertB = tri.vertB;
        Vector3 vertC = tri.vertC;

        // The normal vector of the plane defined by the tri
        Vector3 norm = Vector3.Normalize(Vector3.Cross(vertB - vertA, vertC - vertA));

        // check distance to plane ABC
        float distance = Vector3.Dot(vertA - rayPos, norm) / Vector3.Dot(rayDir, norm); 
        if (distance <= 0 || distance > shortestDistance)
            return false;

        // now it is core algorithm
        {
            goToCoreLoop = true;

            // The intersection in space were the ray intersects the (infinite) plane
            Vector3 I = rayPos + distance * rayDir; 

            // Convert to barycentric coordinates
            // This will find if the intersection is actually within the triangle
            float triangleArea = Vector3.Dot(norm, Vector3.Cross(vertB - vertA, vertC - vertA));
            float areaIBC = Vector3.Dot(norm, Vector3.Cross(vertB - I, vertC - I));

            float baryA = areaIBC / triangleArea;
            if (baryA <= 0)
                return false;

            float areaICA = Vector3.Dot(norm, Vector3.Cross(vertC - I, vertA - I));

            float baryB = areaICA / triangleArea;
            if (baryB <= 0)
                return false;

            float baryC = 1 - baryA - baryB;
            if (baryC <= 0)
                return false; 

            // confirmed it is a valid intersection
            shortestDistance = distance;
            intersection = I;
            return true;
        }
    }

    void ConvertRayResultsToPointResults()
    {
        for (int rayId = 0; rayId < rayResults.ValidCount; rayId++)
        {
            Vector3 rayDir = rayDirs.Cache[rayId];
            Vector3 point = rayResults.Cache[rayId].intersection;

            if (pointResults.ContainsKey(rayDir))
            {
                float newDistance = rayResults.Cache[rayId].distance;
                float newDistanceSqr = newDistance * newDistance;
                float oldDistanceSqr = Vector3.SqrMagnitude(rayPos - pointResults[rayDir]);

                if (newDistanceSqr < oldDistanceSqr)
                {
                    pointResults[rayDir] = point;
                }
            }
            else
            {
                pointResults[rayDir] = point;
            }
        }
    }


    /*
     * aids functions
     */

    void PrintIndexInfos()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("Total detected objects number = " + indexInfos.ValidCount + "\n\n");
        for (int i = 0; i < indexInfos.ValidCount; i++)
        {
            KIndexInfo info = indexInfos.Cache[i];
            sb.Append(detectedObjectNameList[i]);
            sb.Append(info.ToString());

            PrintCoreComputeTimesInRangeOfRayID(info.r_start, info.r_end, ref sb);

            sb.Append("\n\n");
        }
        Debug.Log(sb.ToString());
    }

    void PrintCoreComputeTimesInRangeOfRayID(int rayStartId, int rayEndId, ref StringBuilder sb)
    {
        uint computeDistanceTotalTimes = 0;
        uint computeIntersectionTotalTimes = 0;

        for (int i = rayStartId; i < rayEndId; i++)
        {
            KRayResult ray = rayResults.Cache[i];

            // compute
            computeDistanceTotalTimes += ray.computeDistanceTimes;
            computeIntersectionTotalTimes += ray.computeIntersectionTimes;
        }

        // statistic
        sb.Append("\ncomputeDistanceTotalTimes (1 normalize + 1 cross + 2 dot) = " + computeDistanceTotalTimes / 1000.0f + " k times" +
            "\ncomputeIntersectionTotalTimes (3 cross + 3 dot) = " + computeIntersectionTotalTimes / 1000.0f + " k times");
    }

    void PrintRayResults()
    {
        uint errorCodeOneSum = 0;
        uint errorCodeTwoSum = 0;
        uint errorCodeThreeSum = 0;
        uint validRayNum = 0;
        uint invalidRayNum = 0;
        uint computeDistanceTotalTimes = 0;
        uint computeIntersectionTotalTimes = 0;
        StringBuilder sbValid = new StringBuilder();
        StringBuilder sbInvalid = new StringBuilder();

        for (int i = 0; i < rayResults.ValidCount; i++)
        {
            KRayResult ray = rayResults.Cache[i];

            // compute
            computeDistanceTotalTimes += ray.computeDistanceTimes;
            computeIntersectionTotalTimes += ray.computeIntersectionTimes;

            // error code
            if (ray.errorCode == 1)
                errorCodeOneSum++;
            else if (ray.errorCode == 2)
                errorCodeTwoSum++;
            else if (ray.errorCode == 3)
                errorCodeThreeSum++;

            if (ray.intersection != Vector3.zero)
            {
                validRayNum++;
                sbValid.Append("[rayId = " + i + "] [error code = " + ray.errorCode + "] [intersection = " + ray.intersection + "] [loop = " + ray.computeDistanceTimes + "] [distance = " + ray.distance + "]\n");
            }
            else
            {
                invalidRayNum++;
                sbInvalid.Append("[rayId = " + i + "] [error code = " + ray.errorCode + "] [intersection = " + ray.intersection + "] [loop = " + ray.computeDistanceTimes + "] [distance = " + ray.distance + "]\n");
            }
        }

        sbValid.Insert(0, "Valid ray number = " + validRayNum.ToString() + "\n\n");
        sbInvalid.Insert(0, "Invalid ray number = " + invalidRayNum.ToString() + "\n\n");
        Debug.Log(sbValid.ToString());
        Debug.Log(sbInvalid.ToString());

        // statistic
        Debug.Log("Statistic:  \n\nTotal ray number = " + rayResults.ValidCount +
            "\n\nvertices total number = " + vertices.ValidCount +
            "\n\ntriangles (index) total number = " + triangles.ValidCount +
            "\n\ncomputeDistanceTotalTimes (1 normalize + 1 cross + 2 dot) = " + computeDistanceTotalTimes / 1000000.0f + " million times" +
            "\n\ncomputeIntersectionTotalTimes (3 cross + 3 dot) = " + computeIntersectionTotalTimes / 1000000.0f + " million times" +
            "\n\nErrorCode : \n  id_error = " + errorCodeOneSum +
            "\n  big_than_1000_loops_error = " + errorCodeTwoSum +
            "\n  id_out_range_error = " + errorCodeThreeSum +
            "\n\n"
        );
    }
}
