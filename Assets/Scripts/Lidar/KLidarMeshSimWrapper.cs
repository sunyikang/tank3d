﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class KLidarMeshSimWrapper : MonoBehaviour
{
    /*
     * public input
     */

    public int rayRoundCount = 1024;
    public int rayBeamCount = 32;
    public float rayBeamAngle = 90.0f;
    public int rayRadius = 30;
    public float pointCubeSize = 0.1f;
    public bool drawPointCloud = true;
    public bool drawValidRay = false;
    public bool ignoreDuplicateRay = false;
    public bool printRayDebugInfo = false;
    public bool computeByGPU = true;
    public ComputeShader shader;

    KLidarMeshSim lidar;


    /*
     * constructor
     */

    void OnEnable()
    {
        if (lidar == null)
            lidar = new KLidarMeshSim(shader);
    }

    void OnDisable()
    {
        if (lidar != null)
            lidar.Reset();
    }

    void OnDestroy()
    {
        lidar.Destroy();
    }

    void Update()
    {
        lidar.Update(this.transform.position, 
            rayRoundCount, 
            rayBeamCount, 
            rayBeamAngle, 
            rayRadius,
            computeByGPU,
            ignoreDuplicateRay);

        if (drawValidRay)
            lidar.DrawValidRays();

        if (printRayDebugInfo)
            lidar.PrintRayDebugInfo();
    }

    void OnDrawGizmosSelected()
    {
        if (lidar != null && drawPointCloud)
        {
            lidar.DrawPointCloud(pointCubeSize);
        }
    }
}
