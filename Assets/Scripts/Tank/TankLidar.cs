﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Complete
{
    public class TankLidar : MonoBehaviour
    {
        Ray ray;
        Ray oldRay;

        void UpdateRay()
        {
            Vector3 pos = transform.position;
            pos.y = 1.0f;
            ray = new Ray(pos, transform.forward);
            Debug.DrawRay(ray.origin, ray.direction * 30, Color.green);
        }

        void Update()
        {
            UpdateRay();

            RaycastHit hit;

            if (ray.direction != oldRay.direction)
            {
                oldRay = ray;

                if (Physics.Raycast(ray, out hit, 30))
                {
                    //Debug.Log(gameObject.name + " hit " + hit.transform.gameObject.name + " with distance " + hit.distance);
                }
            }
        }
    }
}